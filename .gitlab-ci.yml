---

include:
  - 'templates/stages.yml'
  # - 'templates/job/generic/lint.yml'
  - 'templates/job/generic/info.yml'
  - 'templates/job/leaks/info.yml'
  - 'templates/job/leaks/lint.yml'
  - 'templates/job/markdown/lint.yml'
  - 'templates/job/changelog/info.yml'
  # - 'templates/job/changelog/deploy.yml'
  - 'templates/job/ansible/lint.yml'
  - 'templates/job/ansible/build.yml'
  - 'templates/job/ansible/test.yml'
  - 'templates/job/ansible/deploy.yml'
  - 'templates/job/asciidoctor/info.yml'
  - 'templates/job/asciidoctor/build.yml'
  - 'templates/job/asciidoctor/package.yml'
  - 'templates/job/asciidoctor/deploy.yml'
  - 'templates/job/dependency-check/info.yml'
  - 'templates/job/docker/lint.yml'
  - 'templates/job/docker/build.yml'
  - 'templates/job/docker/test.yml'
  - 'templates/job/docker/deploy.yml'
  - 'templates/job/latex/build.yml'
  - 'templates/job/terraform/lint.yml'
  - 'templates/job/terraform/build.yml'
  - 'templates/job/terraform/test.yml'
  - 'templates/job/terraform/deploy.yml'

# --------[ ansible ]--------
lint:ansible:
  variables:
    ansible_playbook_file: "test/ansible/playbook.yml"
    ansible_requirements_file: "test/ansible/requirements.yml"

build:ansible-ping:
  variables:
    ansible_inventory_file: "test/ansible/inventory.yml"
    ANSIBLE_FILTER: "all"

build:ansible-var:
  variables:
    ansible_extra_var: "{\"lastname\":\"toto\",\"firstname\":\"tutu\"}"

test:ansible-check:
  variables:
    ansible_inventory_file: "test/ansible/inventory.yml"
    ansible_playbook_file: "test/ansible/playbook.yml"
    ansible_requirements_file: "test/ansible/requirements.yml"
    ANSIBLE_FILTER: "all"

deploy:ansible:
  variables:
    ansible_inventory_file: "test/ansible/inventory.yml"
    ansible_playbook_file: "test/ansible/playbook.yml"
    ansible_requirements_file: "test/ansible/requirements.yml"
    ANSIBLE_FILTER: "all"

# --------[ asciidoctor ]--------
info:asciidoctor-changelog:
  variables:
    asciidoctor_source_path: test/asciidoctor
    asciidoctor_changelog_destination_filename: CHANGELOG

info:asciidoctor-diagrams:
  extends: info:asciidoctor-changelog
  variables:
    diagrams_python_script_filename: test.py

build:asciidoctor-pdf:
  extends:
    - info:asciidoctor-changelog
    - info:asciidoctor-diagrams
  variables:
    asciidoctor_pdf_destination: exports_pdf
    asciidoctor_pdf_destination_filename: README.pdf
    asciidoctor_pdf_source_filename: README.adoc

build:asciidoctor-html:
  extends:
    - info:asciidoctor-changelog
    - info:asciidoctor-diagrams
  variables:
    asciidoctor_html_destination: exports_html
    asciidoctor_html_destination_filename: index.html
    asciidoctor_html_source_filename: README.adoc

package:asciidoctor-pdf:
  extends:
    - build:asciidoctor-pdf
  variables:
    package_name: "asciidoctor-test"

package:asciidoctor-html:
  extends:
    - build:asciidoctor-html
  variables:
    package_name: "asciidoctor-test"

deploy:asciidoctor-release:
  extends:
    - package:asciidoctor-pdf

deploy:asciidoctor-html:
  extends:
    - package:asciidoctor-html

# --------[ docker ]--------
lint:docker:
  variables:
    docker_dockerfile_dir_path: "test/docker"
    docker_dockerfile_filename: "Dockerfile"

build:docker:
  variables:
    docker_dockerfile_dir_path: "test/docker"
    docker_dockerfile_filename: "Dockerfile"
    docker_container_image_name: docker-test

test:docker-trivy:
  extends: build:docker

deploy:docker:
  extends: build:docker

# --------[ latex ]--------
build:latex:
  variables:
    latex_source_filename: "test/latex/main.tex test/latex/main2.tex"

# --------[ markdown ]--------
lint:markdown:
  variables:
    lint_markdown_style_file: test/markdown/custom_style.rb

# --------[ terraform ]--------
lint:terraform-tflint:
  variables:
    TERRAFORM_TFLINT_DIR_PATH: test/terraform
    TFLINT_LOG: debug

lint:terraform-tfsec:
  variables:
    TERRAFORM_TFSEC_DIR_PATH: test/terraform

lint:terraform-checkov:
  variables:
    TERRAFORM_CHECKOV_DIR_PATH: test/terraform

lint:terraform-terrascan:
  variables:
    TERRAFORM_TERRASCAN_DIR_PATH: test/terraform

build:terraform-init:
  variables:
    terraform_source_dir_path: test/terraform
    terraform_workspace: default

test:terraform-plan:
  extends:
    - build:terraform-init

test:terraform-validate:
  extends:
    - build:terraform-init

deploy:terraform-apply:
  extends:
    - build:terraform-init
