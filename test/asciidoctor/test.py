from diagrams import Cluster, Diagram, Edge, Node
from diagrams.custom import Custom
from diagrams.onprem.database import PostgreSQL

graph_attr = {
    # "bgcolor": "cornflowerblue",
    # "margin":"-2, -2",
    "pad": "0.1,0.1"
}

with Diagram("", graph_attr=graph_attr, show=False, filename="images/test_generale_generated", outformat="svg", direction="TB") as diag:
    # Use graphviz cairo renderer to force image embedding
    diag.dot.renderer = "cairo"

    db = PostgreSQL("DB")
